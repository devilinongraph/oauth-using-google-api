# Oauth using Google API

## Description
The OAuth 2.0 authorization framework is a protocol that allows a user to grant a third-party web site or application access to the user's protected resources, without necessarily revealing their long-term credentials or even their identity.

## Steps
The first thing to do is to clone the repository:

```bash
$ git clone https://gitlab.com/devilinongraph/oauth-using-google-api.git
$ cd oauth-using-google-api
```
Create a virtual environment to install dependencies in and activate it (Windows Only):

```bash
$ py –m venv venv
$ .\venv\Scripts\activate
```

Create a virtual environment to install dependencies in and activate it:

```bash
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```bash
(env)$ pip install -r requirements.txt
```
Note the (env / venv) in front of the prompt. This indicates that this terminal session operates in a virtual environment set up by virtualenv2.

Once pip has finished downloading the dependencies:

To create tables:
```bash
$ py manage.py migrate
```
create superuser to test application
```bash
$ py manage.py createsuperuser
```
To use google oauth you need to create client_id token and secret_id token provided by google follow this link to create tokens:
https://console.developers.google.com/

```bash
(env)$ cd project
(env)$ python manage.py runserver
```
And navigate to [localhost](http://localhost:8000).
